<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Casa extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function interesados()
    {
        return $this->hasMany('App\CasaCliente', 'casa_id');
    }

}
