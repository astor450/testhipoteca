<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CasaCliente extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'casa_id'
    ];
    protected $table = 'casas_clientes';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function casa()
    {
        return $this->belongsTo('App\Casa', 'casa_id');
    }

}
