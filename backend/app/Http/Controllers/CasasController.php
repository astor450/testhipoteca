<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ReallySimpleJWT\Token;
use App\User;
use App\Casa;
use App\CasaCliente;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class CasasController extends Controller
{
    public function index(Request $request)
    {
        $token = Token::validate($request->query('_token'), env('SECRET'));
        if(!$token)
            return response()->json(['message' => 'No autorizado', 'status' => 'error']);
        else
            $token = Token::getPayload($request->input('_token'), env('SECRET'));


        $casas = $token['tipo'] == "Arrendador" ? Casa::where('user_id', $token['user_id'])->get() : Casa::all();

        $casas_arr = [];
        Log::info('token', $token);
        foreach ($casas as $casa) {

            if($token['tipo'] == "Arrendador")
            {
                $interesados = [];
            }

            $casa_arr = $casa->toArray();

            foreach ($casa->interesados as $interesado) {
                if($token['tipo'] == "Arrendador")
                {
                    array_push($interesados, $interesado->usuario->toArray());
                }

                $casa_arr['interesado'] = $interesado->user_id == $token['user_id'] ? true : false;

                if($casa_arr['interesado'])
                {
                    if($casa->interesados->count('id') > 1)
                    {
                        $casa_arr['similares'] = [];
                        foreach ($casa->interesados()->where('user_id','<>',$token['user_id'])->get() as $similar) {
                            array_push($casa_arr['similares'], $similar->usuario->toArray());
                        }
                    }
                }
            }


            if($token['tipo'] == "Arrendador")
            {
                $casa_arr['interesados'] = $interesados;
            }
            array_push($casas_arr, $casa_arr);
        }
        return response()->json($casas_arr);
    }

    public function crear(Request $request)
    {
        $token = Token::validate($request->input('_token'), env('SECRET'));
        if(!$token)
            return response()->json(['message' => 'No autorizado', 'status' => 'error']);
        else
            $token = Token::getPayload($request->input('_token'), env('SECRET'));

        $casa = new Casa();
        $casa->fill($request->input());
        $casa->user_id = $token['user_id'];
        try{
            $casa->save();
            return response()->json(['message' => 'Guardada con éxito', 'status' => 'success']);
        }
        catch(Exception $ex)
        {
            return response()->json(['message' => 'No se ha podido guardar', 'status' => 'error']);
        }
    }

    public function interes(Request $request)
    {
        $token = Token::validate($request->input('_token'), env('SECRET'));
        if(!$token)
            return response()->json(['message' => 'No autorizado', 'status' => 'error']);
        else
            $token = Token::getPayload($request->input('_token'), env('SECRET'));

        $interes = new CasaCliente();
        $interes->user_id = $token['user_id'];
        $interes->casa_id = $request->input('casa_id');
        try{
            $interes->save();
            return response()->json(['message' => 'Te interesa', 'status' => 'success']);
        }
        catch(Exception $ex)
        {
            return response()->json(['message' => 'No se ha podido continuar', 'status' => 'error']);
        }
    }
}
