<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ReallySimpleJWT\Token;
use App\User;
use Exception;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function get_current_user(Request $request)
    {
        if($request->input('_token') && Token::validate($request->input('_token'), env('SECRET')))
            return response()->json(['user' => Token::getPayload($request->input('_token'), env('SECRET'))]);
        else
            return response('No autenticado', 401);
    }
    public function registrar(Request $request)
    {
        $user = new User();
        $user->fill($request->input());
        $user->password = Hash::make($request->input('password'));
        $response = [];
        try{
            $user->save();
            $response = [
                'status' => 'success',
                'message' => 'registrado con éxito'
            ];
        }
        catch(Exception $ex){
            $response = [
                'status' => 'error',
                'message' => 'No se ha podido registrar',
                'details' => $ex->getMessage()
            ];
        }
        return response()->json($response);
    }

    public function login(Request $request)
    {
        $user = User::where('correo', $request->input('correo'))->first();
        if(!$user){
            return response()->json(
                [
                    'message' => 'No se ha encontrado el usuario',
                    'status' => 'error'
                ]
                );
        }
        if(!Hash::check($request->input('password'), $user->password))
        {
            return response()->json(
                [
                    'message' => 'Datos incorrectos',
                    'status' => 'error'
                ]
                );
        }
        else{
            $datos_token = [
                'iat' => time(),
                'user_id' => $user->id,
                'exp' => time() + 10000,
                'nombre' => $user->nombre,
                'tipo' => $user->tipo_usuario
            ];
            $token = Token::customPayload($datos_token, env('SECRET'));
            return response()->json(['_token' => $token, 'user' => $datos_token]);
        }



    }
}
