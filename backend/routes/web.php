<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use App\Casa;
use Illuminate\Http\Request;



$router->get('/', function () use ($router) {
    return response()->json(Casa::all());
});
$router->get('usuario', 'UsersController@get_current_user');
$router->post('usuarios', 'UsersController@registrar');
$router->post('login', 'UsersController@login');
$router->post('casas', 'CasasController@crear');
$router->get('casas', 'CasasController@index');
$router->post('casas-interes', 'CasasController@interes');
