# Título del Proyecto

_Test de prueba para el puesto de programador Backend_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Proyecto realizado con Laravel Lumen en el backend y Vanilla Javascript + jQuery en el Frontend, RestFull APP


### Pre-requisitos 📋

_Los requisitos son PHP >= 7.2 y un servidor para el Front, ya sea Nginx o Apache_

Para instalar los requerimientos del backend...

```
cd backend
composer install
```

### Migraciones 🔧

La base de datos del proyecto está preparada para correrse en mysql, no se incluye un archivo .sql
Lumen Tiene soporte para migraciones, así que es necesario modificar el archivo .env del directorio de backend

_Líneas a modificar_

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=hipoteca
DB_USERNAME=root
DB_PASSWORD=12345
```

Una vez esté configurada la base de datos habrá que correr el script de las migraciones

```
cd backend
php artisan migrate
```

_So corre sin errores ya se puede pasar al siguiente paso_

## Ejecutando las pruebas ⚙️

_Para ello tenemos que correr el servidor builtin de PHP_

```
cd backend
php -S 127.0.0.1:5000 -t public
```

Listo, el servidor estará corriendo y se puede ver desde el front en un servidor Apache o Nginx