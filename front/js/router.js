var root = null;
var useHash = true; // Defaults to: false
var hash = '#!'; // Defaults to: '#'
var router = new Navigo(root, useHash, hash);

router
  .on({
    'login': function () {
      $("#contenido").html('');
      $("#contenido").load('login.html');
    },
    'registro': function () {
        $("#contenido").html('');
        $("#contenido").load('registro.html');
    },
    'home': function () {
        $("#contenido").html('');
        $("#contenido").load('home.html');
    },
    'arrendador': function () {
      $("#contenido").html('');
      $("#contenido").load('arrendador.html');
    },
    'cliente': function () {
      $("#contenido").html('');
      $("#contenido").load('cliente.html');
    },
    'logout': function(){
      Cookies.remove('_token');
      Cookies.remove('user');
      router.navigate('login');
      location.reload(true);
    },
    '*': function () {
        router.navigate('home');
    }
  })
  .resolve();